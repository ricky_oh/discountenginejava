package iss.pos.test;

import static org.junit.Assert.*;
import java.io.*;

import org.junit.Test;

import iss.pos.*;
import iss.pos.promotion.*;

public class DiscountTest {

	@Test
	public void test1ABuy1Item() {
		
		//setup
		Promotion promo = new Promotion(); //TODO: setup the promotion as you see fit
		DiscountCalculator dc = new DiscountCalculator(promo);
		
		//Order order = null; //TODO: setup the order, you can refer to SampleTest.java for example
		Order o = new Order();
        o.add(Products.GetProduct("blueDress"), 1);
		//exercise
		Order newOrder = dc.calculateDiscount(o);
		
		//verify
		double expectedValue = 100;//TODO: set the expected value;
		assertEquals(expectedValue, newOrder.getTotalPrice(),0.001);
        //TODO: add additional verification if necessary
	}
	
	@Test
	public void test2ABuy2Item() {
		
		//setup
		Promotion promo = new Promotion(); //TODO: setup the promotion as you see fit
		DiscountCalculator dc = new DiscountCalculator(promo);
		
		//Order order = null; //TODO: setup the order, you can refer to SampleTest.java for example
		Order o = new Order();
        o.add(Products.GetProduct("blueDress"), 2);
		//exercise
		Order newOrder = dc.calculateDiscount(o);
		
		//verify
		double expectedValue = 170;//TODO: set the expected value;
		assertEquals(expectedValue, newOrder.getTotalPrice(),0.001);
        //TODO: add additional verification if necessary
	}
	
	@Test
	public void test3ABuy5Items() {
		
		//setup
		Promotion promo = new Promotion(); //TODO: setup the promotion as you see fit
		DiscountCalculator dc = new DiscountCalculator(promo);
		
		//Order order = null; //TODO: setup the order, you can refer to SampleTest.java for example
		Order o = new Order();
        o.add(Products.GetProduct("blueDress"), 5);
		//exercise
		Order newOrder = dc.calculateDiscount(o);
		
		//verify
		double expectedValue = 440;//TODO: set the expected value;
		assertEquals(expectedValue, newOrder.getTotalPrice(),0.001);
        //TODO: add additional verification if necessary
	}
	
	@Test
	public void test4ABuyxItems() {
		
		//setup
		Promotion promo = new Promotion(); //TODO: setup the promotion as you see fit
		DiscountCalculator dc = new DiscountCalculator(promo);
		
		//Order order = null; //TODO: setup the order, you can refer to SampleTest.java for example
		int x = 0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		String str = null;
		System.out.println("Enter qty to buy"); 
		try{
			str = br.readLine();
		}catch(Exception e){
			e.printStackTrace();
		}
				
		Integer qty = new Integer(str);		

		x = qty.intValue();
		
	
		Order o = new Order();
        o.add(Products.GetProduct("blueDress"), x);
		//exercise
		Order newOrder = dc.calculateDiscount(o);
		
		//verify
		double expectedValue;//TODO: set the expected value;
		expectedValue = 100 * (x/2) + 70 * (x/2) + 100 * (x%2);
		assertEquals(expectedValue, newOrder.getTotalPrice(),0.001);
        //TODO: add additional verification if necessary
	}

}
